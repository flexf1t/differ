function cookies () {
	return;
}

cookies.prototype.CheckCookies = function ()
{
	if (!navigator.cookieEnabled) 
	{
		console.log('Включите cookie для комфортной работы с этим сайтом');
		return false;
	}
	return true;
}

cookies.prototype.GetCookieValueByName = function(name) 
{
	if (this.cookiesEnabled == false) return null;
 	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
 	return matches ? decodeURIComponent(matches[1]) : null;
}

cookies.prototype.GetCookieNamesByValue = function(value) 
{
	if (this.cookiesEnabled == false) return null;
 	var data = document.cookie.split("; ");
 	var result = [];
 	for (var i = 0; i < data.length; i++)
	{
		var temp = data[i].split("=");
		if ((temp.length == 2) && temp[1] == value)
			result.push(temp[0]);
	}
	if (result.length != 0)
 		return result;
 	else
 		return null;
}

cookies.prototype.SetCookie = function (name, value)
{
	if (this.cookiesEnabled == false) return;
	var date = new Date();
	date.setDate(date.getDate() + 7);

	value = encodeURIComponent(value);
	var updatedCookie = name + "=" + value;
	updatedCookie += "; expires=" + date.toUTCString(); 
	document.cookie = updatedCookie;
}

cookies.prototype.DeleteCookie = function (name)
{
	if (this.cookiesEnabled == false) return;
	var date = new Date();
	date.setDate(date.getDate() - 1);
	var updatedCookie = name + "=";
	updatedCookie += "; expires=" + date.toUTCString(); 
	document.cookie = updatedCookie;
}
