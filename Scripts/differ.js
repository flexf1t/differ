﻿(function ()
{  
  function Include(path)
  {
    document.write('<script type="text/javascript" src="../clientgis_main/'+ path + '"></script>');
  }
  function IncludeCSS(path)
  {
    document.write('<link rel="stylesheet" type="text/css" href="http://192.168.64.57/clientgis/' + path + '"/>');
  }
  IncludeCSS("App_Themes/Common/note.css");
  // ExtJs
  // Include("JavaScript/ExtJS/ext-base.js");
  // Include("JavaScript/ExtJS/ext-all.js");
  // Include("JavaScript/ExtJS/BufferView.js");
  // Include("JavaScript/ExtJS/FileUploadField.js");
  // Include("JavaScript/ExtJS/ext-lang-ru.js");
  // ExtJs
  //Include("JavaScript/extsimpleforms.js");//e
  Include("JavaScript/transform.js");//!
  Include("API/Map.js");//!
  Include("JavaScript/Browser.js");//!
  Include("JavaScript/functions.js");//!
  Include("API/DGT/Geometry/Point.js");//!
  Include("API/DGT/ZoomAnimation.js");//!
  Include("API/DGT/Transition.js");//!
  Include("API/DGT/Map.js");//!e
  Include("API/DGT/MapTiles.js");//!
  Include("API/DGT/TileLayers.js");//!
  Include("API/DGT/SingleLayers.js");//!
  Include("API/DGT/Layer.js");//!
  Include("API/DGT/Elements/Element.js");//!
  // Include("API/DGT/Elements/Point.js");
  // Include("API/DGT/Elements/Line.js");
  // Include("API/DGT/Elements/Polygon.js");
  // Include("API/DGT/Elements/Frame.js");
  // Include("API/MapTool.js");
  Include("JavaScript/jsonp.js");//!
  Include("JavaScript/raphael.js");//!
  Include("API/DGT/Nav.js");//!
  // Include("API/DGT/Note.js");//e
  // Include("API/DGT/Edit.js");//e
  Include("API/DGT/CanvasEdit.js");//!
  Include("API/DGT/CanvasLayers.js");//!
  // Include("API/DGT/Controls.js");
  // Include("API/DGT/ControlTool.js");
  // Include("API/DGT/Navigator.js");
  // Include("API/DGT/NavigatorTiles.js");
  // Include("API/DGT/InfoForm.js");//e
  // Include("API/DGT/Select.js");//e
  // Include("API/DGT/ScaleSlider.js");//e
  // Include("API/DGT/ScaleLine.js");
  // Include("API/DGT/Measurement.js");//e
  // Include("API/DGT/Graph.js");//e
  // Include("API/DGT/Print.js");//e
  // Include("API/DGT/DataExchange.js");//e
  // Include("API/DGT/Legend.js");//e
  Include("API/Lang/Lang.js");//!
  InternalParams =
  {
    baseURI: "http://192.168.64.57/clientgis/",
    functions: new Array()
  }
})();


window.onload = function () {
    var d = new differ();
    d.onLoadFinished = function () {
        d.syncEvents();
        d.syncMaps();
    };
    d.load();
    return;
};

function differ() {
    this.divRM = document.getElementById("rightMap");
    this.divLM = document.getElementById("leftMap");
    this.cursorClone = document.getElementById("cursorClone");
    this.rightMap = new Map(this.divRM, { tiles: true });
    this.leftMap = new Map(this.divLM, { tiles: true });
    this.RMLoaded = false;
    this.LMLoaded = false;
    this.RMBlocked = false;
    this.LMBlocked = false;
    this.RMExtent = false;
    this.LMExtent = false;
    this.enumCooTypes = { geog: "geog", squares: "squares", geogGMS: "geogGMS" };
    this.cooType = this.enumCooTypes.geogGMS; 

    this.zoomSyncEnabled = true;
    this.moveSyncEnabled = true;
    this.syncByExtentEnabled = false;

    var thisObj = this;
    this.rightMap.OnLoadImage = function () {
        if (!thisObj.RMLoaded)
        {
            thisObj.RMLoaded = true;
            if (thisObj.RMLoaded && thisObj.LMLoaded && thisObj.onLoadFinished)
                thisObj.onLoadFinished();
        }
        else
        {
            thisObj.RMblocked = false;
            if ((!thisObj.RMblocked && !thisObj.LMblocked) || (!thisObj.zoomSyncEnabled && !thisObj.moveSyncEnabled))
            {
                thisObj.rightMap.UserUnblockMap();
                thisObj.leftMap.UserUnblockMap();
            }
        }
    };
    this.leftMap.OnLoadImage = function () {
        if (!thisObj.LMLoaded)
        {
            thisObj.LMLoaded = true;
            if (thisObj.RMLoaded && thisObj.LMLoaded && thisObj.onLoadFinished)
                thisObj.onLoadFinished();
        }
        else
        {
            thisObj.LMblocked = false;
            if ((!thisObj.RMblocked && !thisObj.LMblocked) || (!thisObj.zoomSyncEnabled && !thisObj.moveSyncEnabled))
            {
                thisObj.rightMap.UserUnblockMap();
                thisObj.leftMap.UserUnblockMap();
            }
        }
    };
    this.rightMap.OnBlock = function () {
        if (!thisObj.RMLoaded || (!thisObj.zoomSyncEnabled && !thisObj.moveSyncEnabled))
            return;
        thisObj.RMblocked = true;
        thisObj.rightMap.UserBlockMap();
    };
    this.leftMap.OnBlock = function () {
        if (!thisObj.LMLoaded || (!thisObj.zoomSyncEnabled && !thisObj.moveSyncEnabled))
            return;
        thisObj.LMblocked = true;
        thisObj.leftMap.UserBlockMap();
    };
    this.rightMap.OnExtentChange = function (extent) {
        if (!thisObj.syncByExtentEnabled || !thisObj.RMLoaded) return;
        if (thisObj.LMExtent)
        {
            thisObj.LMExtent = false;
        }
        else
        {
            thisObj.leftMap.SetExtent(extent);
            thisObj.RMExtent = true;
        }
        return;
    };
    this.leftMap.OnExtentChange = function (extent) {
        if (!thisObj.syncByExtentEnabled || !thisObj.LMLoaded) return;
        if (thisObj.RMExtent)
        {
            thisObj.RMExtent = false;
        }
        else
        {
            thisObj.rightMap.SetExtent(extent);
            thisObj.LMExtent = true;
        }
        return;
    };
    this.rightMap.OnMouseMove = this.getMouseMoveF(document.getElementById("rlat"), document.getElementById("rlon"));
    this.leftMap.OnMouseMove = this.getMouseMoveF(document.getElementById("llat"), document.getElementById("llon"));
    this.setupResizeBar();
    this.moveDivs();
    this.checkURLParams();
    this.setupSettings();
    this.setupCookies();
}

differ.prototype.syncEvent = function (s, d, names, addClientWidth, minusCW) {
    var thisObj = this;
    var getEventListenerFunction = function(d, name, addClientWidth, minusCW) 
    {
        var f = function (e) {
            var additionalWidth = (minusCW ? -1 : 1) * (addClientWidth ? (this.clientWidth + d.clientWidth) / 2 : 0);
            if (name == 'mousemove' && thisObj.cursorClone)
            {
                thisObj.cursorClone.style.left = e.clientX + additionalWidth;
                thisObj.cursorClone.style.top = e.clientY;
                if ((e.clientX + additionalWidth + 16) > document.body.clientWidth)
                    value = document.body.clientWidth - e.clientX - additionalWidth;
                else if ((e.clientX + additionalWidth) <= parseInt(window.getComputedStyle(d.parentElement).left))
                    value = 0;//- newEvent.clientX - parseInt(window.getComputedStyle(d.parentElement).left);
                else if ((e.clientX + additionalWidth + 16) > parseInt(window.getComputedStyle(d.parentElement).left) + d.clientWidth)
                    value = parseInt(window.getComputedStyle(d.parentElement).left) + d.clientWidth - e.clientX - additionalWidth;
                else
                    value = 16;
                thisObj.cursorClone.style.width = ((value < 0) ? 0 : value) + "px";
                if ((e.clientY + 23) > document.body.clientHeight)
                    thisObj.cursorClone.style.height = (document.body.clientHeight - e.clientY) + "px";
                else 
                    thisObj.cursorClone.style.height = 23 + "px";
            }
            if (name == 'mousewheel' || name == 'DOMMouseScroll')
            {
                if (!thisObj.zoomSyncEnabled)
                    return;
            }
            else if (!thisObj.moveSyncEnabled)
                return;
            if (thisObj.syncByExtentEnabled)
                return;
            var newEvent = new Event(name);
            newEvent.wheelDelta = e.wheelDelta;
            newEvent.detail = e.detail;
            newEvent.pageX = e.pageX + additionalWidth;
            newEvent.pageY = e.pageY;
            newEvent.clientX = e.clientX + additionalWidth;
            newEvent.clientY = e.clientY;
            newEvent.button = e.button;
            //experimental
            // if (!thisObj.zoomSyncEnabled)
            // {
            //     var RMscale = thisObj.rightMap.GetScale();
            //     var LMscale = thisObj.leftMap.GetScale();
            //     var k = RMscale / LMscale;
            //     newEvent.pageX = e.pageX * k + additionalWidth + ((k < 1) ? (d.clientWidth / (2 / k)) : 0);
            //     newEvent.pageY = e.pageY * k + ((k < 1) ? (d.clientHeight / (2 * k)) : 0);
            //     newEvent.clientX = e.clientX * k + additionalWidth + ((k < 1) ? (d.clientWidth / (2 / k)) : 0);
            //     newEvent.clientY = e.clientY * k + ((k < 1) ? (d.clientHeight / (2 / k)) : 0);
            // }
            document.createEvent ? d.dispatchEvent(newEvent) : d.fireEvent("on" + e.eventType, newEvent);     
        };
        return f;
    };
    for (var i = 0; i < names.length; i++)
        s.addEventListener(names[i], getEventListenerFunction(d, names[i], addClientWidth, minusCW));
};

differ.prototype.load = function () {
    this.rightMap.LoadAtlas("worldmapwgs84\\worldmap.fra");
    //this.leftMap.LoadAtlas("worldmapwgs84\\worldmap.fra");
    this.leftMap.LoadAtlas("P_Russ\\russ5sub.fra");  
};

differ.prototype.syncEvents = function () {
    var thisObj = this;
    var s = function (s, d, minusCW) {
        thisObj.syncEvent(s, d, ['DOMMouseScroll', 'mousewheel', 'mousemove'], true, minusCW);
        thisObj.syncEvent(s, d, ['mousedown', 'mouseup']);
    };
    //RM -> LM
    s(this.divRM, this.divLM.children[0]);
    //LM -> RM
    s(this.divLM, this.divRM.children[0], true);
};

differ.prototype.syncScale = function () {
    if (!this.RMLoaded || !this.LMLoaded)
        return;
    var RMscale = this.rightMap.GetScale();
    var LMscale = this.leftMap.GetScale();
    if (RMscale != LMscale)
    {
        var newScale = (RMscale + LMscale) / 2;
        this.rightMap.SetScale(newScale);
        this.leftMap.SetScale(newScale);
    }
};

differ.prototype.syncPos = function () {
    if (!this.RMLoaded || !this.LMLoaded)
        return;
    var RMcenter = {
        x: (this.rightMap.GetExtent().xmin + this.rightMap.GetExtent().xmax) / 2,
        y: (this.rightMap.GetExtent().ymin + this.rightMap.GetExtent().ymax) / 2,
    };
    var LMcenter = {
        x: (this.leftMap.GetExtent().xmin + this.leftMap.GetExtent().xmax) / 2,
        y: (this.leftMap.GetExtent().ymin + this.leftMap.GetExtent().ymax) / 2,
    };
    
    if (RMcenter.x != LMcenter.x || RMcenter.y != LMcenter.y)
    {
        this.rightMap.CenterAt((RMcenter.x + LMcenter.x) / 2, (RMcenter.y + LMcenter.y) / 2);
        this.leftMap.CenterAt((RMcenter.x + LMcenter.x) / 2, (RMcenter.y + LMcenter.y) / 2);
    }
};

differ.prototype.syncMaps = function () {
    this.syncScale();
    this.syncPos();
};

differ.prototype.moveDivs = function () {
    var bar = document.getElementById("lbar");
    var lid = document.getElementById("lid");
    var value = (parseInt(window.getComputedStyle(this.divLM).left) + 5) + 'px';
    bar.style.left = value;
    lid.style.left = value;
};

differ.prototype.setupCookies = function () {
    this.cookies = new cookies();
    this.cookies.GetCookieValueByName("cursorSetting") ? this.settings.setState("cursorSetting", false) : this.settings.setState("cursorSetting", true);
    this.cookies.GetCookieValueByName("zoomSetting") ? this.settings.setState("zoomSetting", false) : this.settings.setState("zoomSetting", true);
    this.cookies.GetCookieValueByName("moveSetting") ? this.settings.setState("moveSetting", false) : this.settings.setState("moveSetting", true);
    this.cookies.GetCookieValueByName("extentSetting") ? this.settings.setState("extentSetting", true) : this.settings.setState("extentSetting", false);
    var thisObj = this;
};

differ.prototype.setupSettings = function () {
    this.settings = new settings();
    this.settings.setupButton();
    this.settings.setupCheckBoxes();
    var thisObj = this;
    this.settings.onCheckBoxChange = function (args) {
        if (args.id === 'cursorSetting')
        {
            var c = document.getElementById("cursorClone");
            if (!c) return;
            args.checked ? thisObj.cursorClone = c : thisObj.cursorClone = null;
            args.checked ? c.style.visibility = "visible" : c.style.visibility = "hidden";
        }
        else if (args.id === 'zoomSetting')
        {
            thisObj.zoomSyncEnabled = args.checked;
            if (args.checked) thisObj.syncScale();
        }
        else if (args.id === 'moveSetting')
        {
            thisObj.moveSyncEnabled = args.checked;
            if (args.checked) thisObj.syncPos();
        }
        else if (args.id === 'extentSetting')
        {
            thisObj.syncByExtentEnabled = args.checked;
            thisObj.settings.setEnable("zoomSetting", !args.checked);
            thisObj.settings.setEnable("moveSetting", !args.checked);
        }
        if (thisObj.cookies) 
            args.checked ? thisObj.cookies.DeleteCookie(args.id) : thisObj.cookies.SetCookie(args.id, false);
    };
};

differ.prototype.setupResizeBar = function () {
    var thisObj = this;
    this.startX = 0;
    this.startY = 0;
    this.startWidth = 0;
    this.startHeight = 0;
    this.resiziginItem = document.getElementById("resizeBar");
    function initDrag(e) {
        thisObj.startX = e.clientX;
        thisObj.startWidth = parseInt(window.getComputedStyle(thisObj.resiziginItem).left);
        document.documentElement.addEventListener('mousemove', doDrag, false);
        document.documentElement.addEventListener('mouseup', stopDrag, false);
        thisObj.divRM.style.cursor = "url(Images/arrowResize.png) 8 8, auto";
        thisObj.divLM.style.cursor = "url(Images/arrowResize.png) 8 8, auto";
        return false;
    }
    function doDrag(e) {
        var newPercent = (thisObj.startWidth + (e.clientX - thisObj.startX)) / document.body.clientWidth * 100;
        thisObj.resiziginItem.style.left = newPercent + '%';//(thisObj.startWidth + (e.clientX - thisObj.startX)) + 'px';
        thisObj.divRM.style.width = newPercent + '%';//(thisObj.startWidth + (e.clientX - thisObj.startX)) + 'px';
        thisObj.divLM.style.left = newPercent + '%';//(thisObj.startWidth + (e.clientX - thisObj.startX)) + 'px';
        thisObj.divLM.style.width = (100 - newPercent) + '%';//(document.body.clientWidth - thisObj.startWidth - (e.clientX - thisObj.startX)) + 'px';
        return false;
    }
    function stopDrag(e) {
        document.documentElement.removeEventListener('mousemove', doDrag, false);
        document.documentElement.removeEventListener('mouseup', stopDrag, false);
        thisObj.divRM.style.cursor = "";
        thisObj.divLM.style.cursor = "";
        thisObj.leftMap.Resize();
        thisObj.rightMap.Resize();
        if (thisObj.zoomSyncEnabled)
            thisObj.syncScale();
        if (thisObj.moveSyncEnabled)
            thisObj.syncPos();
        thisObj.moveDivs();
        return false;
    }
    this.resiziginItem.addEventListener('mousedown', initDrag, false);
};

differ.prototype.checkURLParams = function ()
{
    var rid = GetURLParamVal("rid");
    var lid = GetURLParamVal("lid");
    var div;
    if (rid !== null)
    {
        div = document.getElementById("rid");
        if (div) div.innerHTML = rid;
    }
    if (lid !== null)
    {
        div = document.getElementById("lid");
        if (div) div.innerHTML = lid;
    }
    return false;
};

differ.prototype.getMouseMoveF = function (elLat, elLon) {
    return Util.Bind(function (x, y, sx, sy) {
        if (this.cooType == this.enumCooTypes.geog)
        {
            if (x && y)
            {
                elLat.innerHTML = x.toFixed(8);
                elLon.innerHTML = y.toFixed(8);
            }
            else
            {
                elLat.innerHTML = "";
                elLon.innerHTML = "";
            }
        }
        else if (this.cooType == this.enumCooTypes.geogGMS)
        {
            if (x && y)
            {
                x = Number(x);
                y = Number(y);
                var sphX = "N";
                if (x < 0)
                    sphX = "S";
                var sphY = "E";
                if (y < 0)
                    sphY = "W";
                var lat = FloatToDegree(x);
                var lon = FloatToDegree(y);
                elLat.innerHTML = lat.deg + "&#176;" + lat.min + "'" + lat.sec + "''" + sphX;
                elLon.innerHTML = lon.deg + "&#176;" + lon.min + "'" + lon.sec + "''" + sphY;
                round = null;
            }
            else
            {
                elLat.innerHTML = "";
                elLon.innerHTML = "";
            }
        }
        else if (this.cooType == this.enumCooTypes.squares)
        {
            if (sx && sy)
            {
                elLat.innerHTML = sx;
                elLon.innerHTML = sy;
            }
            else
            {
                elLat.innerHTML = "";
                elLon.innerHTML = "";
            }
        }
    }, this);
};