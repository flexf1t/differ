function settings () {
    this.settingsButton = document.getElementById("settingsButton");
    this.settingsPanel = document.getElementById("settingsPanel");
    this.timer;
    this.degree = 0;
    this.direction = 'clockwise';
    this.visible = false;
    var thisObj = this;
    this.rotateFunction = function () {
    	if (!thisObj.settingsButton)
    		return;
        (thisObj.direction == 'clockwise') ? thisObj.degree++ : thisObj.degree--;
        if (thisObj.degree >= 360 || thisObj.degree <= -360)
            thisObj.degree = 0;
       thisObj.settingsButton.style.transform = 'rotate(' + thisObj.degree + 'deg)';
    };
}

settings.prototype.setupButton = function () {
	if (!this.settingsButton || !this.settingsPanel)
		return;
	var thisObj = this;
    this.settingsButton.addEventListener("mouseover", function (e) {
        if (!thisObj.visible)
            thisObj.timer = window.setInterval(thisObj.rotateFunction, 75);
    });
    this.settingsButton.addEventListener("mouseout", function (e) {
        if (!thisObj.visible)
        {
            clearInterval(thisObj.timer);
            thisObj.timer = null;
        }
    });
    this.settingsButton.addEventListener("mousedown", function (e) {
        if (thisObj.visible)
        {
            thisObj.visible = false;
            thisObj.settingsPanel.style.visibility = "hidden";
            clearInterval(thisObj.timer);
            thisObj.direction = 'clockwise';
            thisObj.timer = null;
        }
        else
        {
            thisObj.visible = true;
            thisObj.direction = 'counterclockwise';
            if (!thisObj.timer)
                thisObj.timer = window.setInterval(thisObj.rotateFunction, 75);
            thisObj.settingsPanel.style.visibility = "visible";
        }
    });
};

settings.prototype.setState = function (id, state) {
	if (!this.settingsPanel)
		return;
	this.checkBoxes = document.getElementsByClassName('setting');
	for (var i = 0; i < this.checkBoxes.length; i++)
	{
		if (this.checkBoxes[i].id == id)
		{
			this.checkBoxes[i].firstElementChild.checked = state;
			if (this.onCheckBoxChange)
				this.onCheckBoxChange({
					item: this.checkBoxes[i],
					checked: state,
					id: this.checkBoxes[i].id
				});
			return;
		}
	}
};

settings.prototype.setEnable = function (id, enable) {
    if (!this.settingsPanel)
        return;
    this.checkBoxes = document.getElementsByClassName('setting');
    for (var i = 0; i < this.checkBoxes.length; i++)
    {
        if (this.checkBoxes[i].id == id)
        {
            this.checkBoxes[i].firstElementChild.disabled = !enable;
            return;
        }
    }
};


settings.prototype.setupCheckBoxes = function () {
	if (!this.settingsPanel)
		return;
	this.checkBoxes = document.getElementsByClassName('setting');
	var thisObj = this;
	var f = function (e) {
		if (thisObj.onCheckBoxChange)
			thisObj.onCheckBoxChange({
				item: this,
				checked: this.firstElementChild ? this.firstElementChild.checked : null,
				id: this.id
			});
	};
	for (var i = 0; i < this.checkBoxes.length; i++)
	{
		this.checkBoxes[i].addEventListener('change', f);
	}
};